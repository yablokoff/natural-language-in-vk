import math
import scipy


def pearson_correlation(u, v):
    return math.sqrt(2 * (1 - scipy.stats.pearsonr(u, v)[0]))
