import dataset
import gensim
import math
import numpy
from numpy import linalg


class ArticlesTest(object):

    def __init__(self, lsa_dims=10):
        self.lsa_dims = lsa_dims
        gc = dataset.DatasetCreator(source_file="../sources/articles_test/articles.txt", below_rank=2, above_rank=1)
        tfidf_corpus = gc.model[gc.corpus]
        tfidf_docs = []
        for doc in tfidf_corpus:
            term_vector = gensim.matutils.sparse2full(doc, len(gc.dictionary))
            tfidf_docs.append(term_vector)
        self.tfidf_docs = tfidf_docs
        self.tfidf_2d = numpy.vstack(tfidf_docs).transpose()

    def cosine_similarity(self, u, v):
        return numpy.dot(u, v) / (math.sqrt(numpy.dot(u, u)) * math.sqrt(numpy.dot(v, v)))

    def term_tfidf_sim_csv(self):
        term_rows = []
        for term1 in self.tfidf_2d:
            term_rows.append(",".join([str(self.cosine_similarity(term1, term2)) for term2 in self.tfidf_2d]))
        return term_rows

    def term_lsa_sim_csv(self):
        term_rows = []
        U, s, V = linalg.svd(self.tfidf_2d, full_matrices=False)
        lsa_2d = numpy.dot(U[:, :self.lsa_dims], numpy.diag(s)[:self.lsa_dims, :self.lsa_dims])
        for term1 in lsa_2d:
            term_rows.append(",".join([str(self.cosine_similarity(term1, term2)) for term2 in lsa_2d]))
        return term_rows

    def doc_lsa_sim_csv(self):
        doc_rows = []
        U, s, V = linalg.svd(self.tfidf_2d, full_matrices=False)
        lsa_2d = numpy.dot(V[:, :self.lsa_dims], numpy.diag(s)[:self.lsa_dims, :self.lsa_dims])
        for doc1 in lsa_2d:
            doc_rows.append(",".join([str(self.cosine_similarity(doc1, doc2)) for doc2 in lsa_2d]))
        return doc_rows
