from collections import defaultdict
import logging
import numpy
import sys
sys.setrecursionlimit(100000)

from clustering_managers import IClusterer
import evaluator
import hcluster

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger("nl-vk.clustering_managers.gaac_managers")


class GAACClustererManager(IClusterer):
    def __init__(self, tfifd_docs, svd_dimensions,
                 linkage_method="single", distance_threshold=0.9, *args, **kwargs):
        self.linkage_method = linkage_method
        self.distance_threshold = distance_threshold
        self.dict = kwargs["dict"]
        super(GAACClustererManager, self).__init__(tfifd_docs, svd_dimensions)

    def get_clusterer(self, *args, **kwargs):
        pass

    def run(self):
        logger.info("Run GAAC with linkage method: %s" % self.linkage_method)
        dists = hcluster.pdist(self.tfidf_docs, "cosine")
        self.linkage = numpy.clip(hcluster.linkage(dists, method=self.linkage_method), 0, 10 ** 10)
        hcluster.dendrogram(self.linkage, truncate_mode="level", p=7, show_contracted=True)
        logger.info("Calculated GAAC with linkage method: %s" % self.linkage_method)

    def _create_clusters(self):
        clusters_lists = defaultdict(list)
        docs_clusters = dict()
        docs_clusters_list = hcluster.fcluster(self.linkage, self.distance_threshold, criterion="distance")
        for doc_num, cluster_index in enumerate(docs_clusters_list):
            clusters_lists[cluster_index].append(doc_num)
            docs_clusters[doc_num] = cluster_index
        logger.info("Clusters num: %d" % len(clusters_lists))
        return clusters_lists, docs_clusters

    def _create_clusters_means(self, clusters_lists):
        means = []
        for cluster_index, cluster_elems in clusters_lists.iteritems():
            cluster_docs_sum = self.tfidf_docs[cluster_elems[0]]
            for doc_index in cluster_elems[1:]:
                cluster_docs_sum += self.tfidf_docs[doc_index]
            means.append(cluster_docs_sum / float(len(cluster_elems)))
        return means

    def evaluate(self):
        logger.info("Evaluate clusters quality")
        classes_lists, docs_classes = self._create_classes()
        clusters_lists, docs_clusters = self._create_clusters()
        means = self._create_clusters_means(clusters_lists)

        internal_evaluation, vbeta_evaluation, vbeta_smart_evaluation, rand_index_evaluation, purity_evaluation = \
            self.evaluation_rates(means=means, classes_lists=classes_lists, clusters_lists=clusters_lists,
                                  docs_classes=docs_classes, docs_clusters=docs_clusters)
        evaluation = internal_evaluation + vbeta_evaluation + rand_index_evaluation + purity_evaluation
        logger.info("Results: %f\t %f\t %f\t %f\t %f" %
                    (internal_evaluation, vbeta_evaluation, vbeta_smart_evaluation,
                     rand_index_evaluation, purity_evaluation))
