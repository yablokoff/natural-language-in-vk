from collections import defaultdict
import evaluator
import numpy
import pickle


class IClusterer(object):
    def __init__(self, tfidf_docs, svd_dimensions=0, *args, **kwargs):
        self.svd_dimensions = svd_dimensions
        if self.svd_dimensions and self.svd_dimensions < len(tfidf_docs[0]):
            try:
                svd_vectors_file = open("../sources/SVDVectors.pk", "rb")
                SVD_matrices = pickle.load(svd_vectors_file)
                S = SVD_matrices["S"]
                T = SVD_matrices["T"]
                Dt = SVD_matrices["Dt"]
            except IOError:
                [u, d, vt] = numpy.linalg.svd(numpy.transpose(numpy.array(tfidf_docs)))
                S = d[:self._svd_dimensions] * \
                    numpy.identity(self._svd_dimensions, numpy.float64)
                T = u[:,:self._svd_dimensions]
                Dt = vt[:self._svd_dimensions,:]
                svd_vectors_file = open("../sources/SVDVectors.pk", "wb")
                pickle.dump({"S": S, "T": T, "Dt": Dt}, svd_vectors_file)
            tfidf_docs = numpy.transpose(numpy.dot(S, Dt))
            self._Tt = numpy.transpose(T)
        self.tfidf_docs = tfidf_docs
        self.meanss = []

    def get_clusterer(self, *args, **kwargs):
        pass

    def _create_classes(self):
        classes_lists = defaultdict(list)
        docs_classes = dict()
        with open("../sources/groups_with_classes.txt", "r") as classes_file:
            for doc_num, class_index in enumerate(classes_file):
                class_index = int(class_index)
                classes_lists[class_index].append(doc_num)
                docs_classes[doc_num] = class_index
        return classes_lists, docs_classes

    def _create_clusters(self, *args, **kwargs):
        pass

    def run(self):
        pass

    def evaluation_rates(self, means, classes_lists, clusters_lists, docs_classes, docs_clusters):
        if means:
            internal_QA = evaluator.InternalEvaluator(means=means, evaluator_func=evaluator.overall_similarity)
            internal_evaluation = internal_QA.evaluate()
        else:
            internal_evaluation = 0
        vbeta_QA = evaluator.VBetaMeasureEvaluator(classes=classes_lists, clusters=clusters_lists,
                                                   docs_count=len(self.tfidf_docs))
        vbeta_evaluation = vbeta_QA.evaluate()
        vbeta_smart_QA = evaluator.VBetaMeasureEvaluator(classes=classes_lists, clusters=clusters_lists,
                                                         docs_count=len(self.tfidf_docs),
                                                         beta=float(len(clusters_lists)) / len(classes_lists))
        vbeta_smart_evaluation = vbeta_smart_QA.evaluate()
        rand_index_QA = evaluator.RandIndexEvaluator(classes=docs_classes, clusters=docs_clusters,
                                                     docs_count=len(self.tfidf_docs))
        rand_index_evaluation = rand_index_QA.evaluate()
        purity_QA = evaluator.PurityEvaluator(classes=classes_lists, clusters=clusters_lists,
                                              docs_count=len(self.tfidf_docs))
        purity_evaluation = purity_QA.evaluate()
        return internal_evaluation, vbeta_evaluation, vbeta_smart_evaluation, rand_index_evaluation, \
            purity_evaluation

    def evaluate(self):
        pass
