from collections import defaultdict
import logging
import nltk

from clustering_managers import IClusterer
from clusterers import kmeansbisecting, kmeans
import evaluator

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger("nl-vk.clustering_managers.kmeans_managers")


class KMeansClustererManager(IClusterer):
    def __init__(self, tfifd_docs, svd_dimensions, distance_func, num_means, *args, **kwargs):
        self.distance_func = distance_func
        self.num_means = num_means
        super(KMeansClustererManager, self).__init__(tfifd_docs, svd_dimensions)

    def get_clusterer(self, *args, **kwargs):
        self.clusterer = kmeans.KMeansModClusterer(num_means=self.num_means, conv_test=0.01,
                                                   distance=self.distance_func, svd_dimensions=self.svd_dimensions,
                                                   repeats=3, avoid_empty_clusters=True)

    def run(self):
        for i in range(5):
            self.get_clusterer()
            logger.info("Run " + str(self.clusterer) + " with distance function " + str(self.distance_func))
            self.clusterer.cluster(self.tfidf_docs)
            self.meanss.append(
                self.clusterer.means()
            )
            logger.info("Calculated " + str(self.clusterer))

    def _create_clusters(self, means):
        """
        For current clustering return dict of lists of docs nums for each cluster, like:
        {
            cluster_index_i: [doc_index_ij, ..., ]
        }
        """
        self.clusterer._means = means
        clusters_lists = defaultdict(list)
        docs_clusters = dict()
        for doc_num, tfidf_doc in enumerate(self.tfidf_docs):
            cluster_index = self.clusterer.classify_vectorspace(tfidf_doc)
            clusters_lists[cluster_index].append(doc_num)
            docs_clusters[doc_num] = cluster_index
        return clusters_lists, docs_clusters

    def evaluate(self):
        logger.info("Evaluate clusters quality")
        logger.info("Internal result; v-0.5; v-beta; rand index; purity")

        classes_lists, docs_classes = self._create_classes()

        index_of_best_means = 0
        evaluation_of_best_means = 0
        for num, means in enumerate(self.meanss):
            clusters_lists, docs_clusters = self._create_clusters(means)

            internal_evaluation, vbeta_evaluation, vbeta_smart_evaluation, rand_index_evaluation, purity_evaluation = \
                self.evaluation_rates(means=means, classes_lists=classes_lists, clusters_lists=clusters_lists,
                                      docs_classes=docs_classes, docs_clusters=docs_clusters)
            evaluation = internal_evaluation + vbeta_evaluation + rand_index_evaluation + purity_evaluation
            if evaluation > evaluation_of_best_means:
                index_of_best_means = num
                evaluation_of_best_means = evaluation
            logger.info("Attempt %d: %f\t %f\t %f\t %f\t %f" %
                        (num, internal_evaluation, vbeta_evaluation, vbeta_smart_evaluation,
                         rand_index_evaluation, purity_evaluation))
        return index_of_best_means


class BisectingKMeansClusteringManager(KMeansClustererManager):
    def get_clusterer(self, *args, **kwargs):
        self.clusterer = kmeansbisecting.KMeansModBisectingClusterer(num_means=self.num_means, bisecting_repeats=3,
                                                                     conv_test=0.01, distance=self.distance_func,
                                                                     repeats=3, avoid_empty_clusters=True)
