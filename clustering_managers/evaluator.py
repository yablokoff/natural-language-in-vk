__author__ = 'yablokoff'

import math
import numpy


def overall_similarity(means):
    sum_len = 0
    for mean in means:
        sum_len += numpy.linalg.norm(mean, ord=2) ** 2
    return sum_len / len(means)


class InternalEvaluator(object):
    def __init__(self, means, evaluator_func):
        self.means = means
        self.evaluator_func = evaluator_func

    def evaluate(self):
        return self.evaluator_func(self.means)


class ExternalEvaluator(object):
    def __init__(self, classes, clusters, docs_count):
        self.classes = classes
        self.clusters = clusters
        self.docs_count = docs_count

    def evaluate(self):
        raise NotImplementedError


class VBetaMeasureEvaluator(ExternalEvaluator):
    def __init__(self, classes, clusters, docs_count, beta=0.5):
        self.beta = beta
        super(VBetaMeasureEvaluator, self).__init__(classes, clusters, docs_count)

    def evaluate(self):
        entropy_cl = 0
        for cluster_j_index, cluster_j_elems in self.clusters.iteritems():
            for class_i_index, class_i_elems in self.classes.iteritems():
                n_ij = set(cluster_j_elems).intersection(class_i_elems)
                if len(n_ij):
                    entropy_cl += len(n_ij) / float(self.docs_count) * \
                        math.log(float(len(n_ij)) / len(cluster_j_elems), 2)
        entropy_cl = -entropy_cl
        entropy_c = 0
        for class_i_index, class_i_elems in self.classes.iteritems():
            entropy_c += float(len(class_i_elems)) / self.docs_count * \
                math.log(float(len(class_i_elems)) / self.docs_count, 2)
        entropy_c = -entropy_c
        h = 1 - entropy_cl / entropy_c

        entropy_l = 0
        for cluster_j_index, cluster_j_elems in self.clusters.iteritems():
            entropy_l += float(len(cluster_j_elems)) / self.docs_count * \
                math.log(float(len(cluster_j_elems)) / self.docs_count, 2)
        entropy_l = -entropy_l
        entropy_lc = 0
        for class_i_index, class_i_elems in self.classes.iteritems():
            for cluster_j_index, cluster_j_elems in self.clusters.iteritems():
                n_ij = set(cluster_j_elems).intersection(class_i_elems)
                if len(n_ij):
                    entropy_lc += len(n_ij) / float(self.docs_count) * \
                        math.log(float(len(n_ij)) / len(class_i_elems), 2)
        entropy_lc = -entropy_lc
        c = 1 - entropy_lc / entropy_l

        v_rate = (1 + self.beta) * h * c / ((self.beta * h) + c)

        return v_rate


class RandIndexEvaluator(ExternalEvaluator):

    def evaluate(self):
        true_positives = 0
        true_negatives = 0
        tp_tn_fp_fn = 0
        for doc_num_i in range(self.docs_count):
            for doc_num_j in range(self.docs_count):
                if doc_num_i > doc_num_j or doc_num_i == doc_num_j:
                    continue
                class_i_index = self.classes[doc_num_i]
                class_j_index = self.classes[doc_num_j]
                cluster_i_index = self.clusters[doc_num_i]
                cluster_j_index = self.clusters[doc_num_j]
                if class_i_index != class_j_index and cluster_i_index != cluster_j_index:
                    true_negatives += 1
                if class_i_index == class_j_index and cluster_i_index == cluster_j_index:
                    true_positives += 1
                tp_tn_fp_fn += 1
        return (float(true_positives) + true_negatives) / tp_tn_fp_fn


class PurityEvaluator(ExternalEvaluator):

    def evaluate(self):
        purity_rate = 0

        for cluster_index, cluster_elems in self.clusters.iteritems():
            max_intersection = 0
            for class_index, class_elems in self.classes.iteritems():
                common_elems = set(cluster_elems).intersection(set(class_elems))
                if len(common_elems) > max_intersection:
                    max_intersection = len(common_elems)
            purity_rate += max_intersection

        return float(purity_rate) / self.docs_count