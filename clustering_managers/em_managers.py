from collections import defaultdict
import logging
import nltk
import numpy
import random
from sklearn import mixture

from clustering_managers import IClusterer
import evaluator

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger("nl-vk.clustering_managers.kmeans_managers")


class EMRandomClusterer(nltk.cluster.EMClusterer):

    def __init__(self, num_means, *args, **kwargs):
        super(EMRandomClusterer, self).__init__(initial_means=list(), **kwargs)
        self._num_clusters = num_means

    def cluster_vectorspace(self, vectors, trace=False):
        self._means = random.Random().sample(vectors, self._num_clusters)
        super(EMRandomClusterer, self).cluster_vectorspace(vectors, trace)


class EMClustererManager(IClusterer):
    def __init__(self, tfifd_docs, num_means, *args, **kwargs):
        self.num_means = num_means
        super(EMClustererManager, self).__init__(tfifd_docs)

    def get_clusterer(self, *args, **kwargs):
        self.clusterer = EMRandomClusterer(num_means=2, conv_threshold=0.1)

    def run(self):
        for i in range(2):
            self.get_clusterer()
            logger.info("Run EM Gaussian " + str(self.clusterer))
            self.clusterer.cluster(self.tfidf_docs)
            self.meanss.append(
                self.clusterer._means
            )
            logger.info("Calculated " + str(self.clusterer))

    def evaluate(self):
        logger.info("Evaluate clusters quality")
        index_of_best_means = 0
        evaluation_of_best_means = 0
        for num, means in enumerate(self.meanss):
            QA = evaluator.InternalEvaluator(means=means, evaluator_func=evaluator.overall_similarity)
            evaluation = QA.evaluate()
            if evaluation > evaluation_of_best_means:
                index_of_best_means = num
                evaluation_of_best_means = evaluation
            logger.info("Attempt %d: result %f" % (num, evaluation))
        return index_of_best_means


class GMMClustererManager(IClusterer):

    def __init__(self, tfifd_docs, svd_dimensions, num_means, *args, **kwargs):
        self.num_means = num_means
        super(GMMClustererManager, self).__init__(tfifd_docs, svd_dimensions)

    def get_clusterer(self, *args, **kwargs):
        self.clusterer = mixture.GMM(n_components=self.num_means, thresh=0.01, covariance_type="diag")

    def run(self):
        for i in range(1):
            self.get_clusterer()
            logger.info("Run EM Gaussian " + str(self.clusterer))
            self.clusterer.fit(self.tfidf_docs)
            logger.info("Calculated " + str(self.clusterer))

    def _create_clusters(self):
        """
        For current clustering return dict of lists of docs nums for each cluster, like:
        {
            cluster_index_i: [doc_index_ij, ..., ]
        }
        """
        clusters_lists = defaultdict(list)
        docs_clusters = dict()
        docs_clusters_list = self.clusterer.predict(self.tfidf_docs)
        for doc_num, cluster_index in enumerate(docs_clusters_list):
            clusters_lists[cluster_index].append(doc_num)
            docs_clusters[doc_num] = cluster_index
        return clusters_lists, docs_clusters

    def _create_clusters_means(self, clusters_lists):
        means = []
        for cluster_index, cluster_elems in clusters_lists.iteritems():
            cluster_docs_sum = self.tfidf_docs[cluster_elems[0]]
            for doc_index in cluster_elems[1:]:
                cluster_docs_sum += self.tfidf_docs[doc_index]
            means.append(cluster_docs_sum / float(len(cluster_elems)))
        return means

    def evaluate(self):
        logger.info("Evaluate clusters quality")
        logger.info("Internal result; v-0.5; v-beta; rand index; purity")

        classes_lists, docs_classes = self._create_classes()

        index_of_best_means = 0
        clusters_lists, docs_clusters = self._create_clusters()
        means = self._create_clusters_means(clusters_lists)

        internal_evaluation, vbeta_evaluation, vbeta_smart_evaluation, rand_index_evaluation, purity_evaluation = \
            self.evaluation_rates(means=means, classes_lists=classes_lists, clusters_lists=clusters_lists,
                                  docs_classes=docs_classes, docs_clusters=docs_clusters)
        logger.info("Rates: %f\t %f\t %f\t %f\t %f" %
                    (internal_evaluation, vbeta_evaluation, vbeta_smart_evaluation,
                     rand_index_evaluation, purity_evaluation))
        return index_of_best_means