# -*- coding: utf-8 -*-

import gensim
import logging
import nltk
import numpy

from clustering_managers import kmeans_managers, gaac_managers, em_managers
import dataset
import printer

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger("nl-vk.clustering")


def run_gaac():
    gc = dataset.DatasetCreator()
    tfidf_corpus = gc.model[gc.corpus]
    tfidf_docs = []
    for doc in tfidf_corpus:
        new_vector = gensim.matutils.sparse2full(doc, len(gc.dictionary))
        if numpy.nonzero(new_vector)[0].size:
            tfidf_docs.append(new_vector)
    clusterer = gaac_managers.GAACClustererManager(tfifd_docs=tfidf_docs,
                                                   svd_dimensions=80,
                                                   linkage_method="average",
                                                   distance_threshold=0.55,
                                                   dict=gc.dictionary)
    clusterer.run()
    return clusterer


def run_lsi():
    gc = dataset.DatasetCreator()
    tfidf_corpus = gc.model[gc.corpus]
    lsi = gensim.models.LsiModel(tfidf_corpus, id2word=gc.dictionary, num_topics=20)
    lsi_corpus = lsi[tfidf_corpus]
    return lsi, lsi_corpus


if __name__ == "__main__":
    gc = dataset.DatasetCreator()
    tfidf_corpus = gc.model[gc.corpus]
    tfidf_docs = []
    for doc in tfidf_corpus:
        new_vector = gensim.matutils.sparse2full(doc, len(gc.dictionary))
        if numpy.nonzero(new_vector)[0].size:
            tfidf_docs.append(new_vector)

    # for num_means in range(12, 37, 4):
    #     em = em_managers.GMMClustererManager(tfifd_docs=tfidf_docs,
    #                                          svd_dimensions=80,
    #                                          num_means=num_means)
    #     em.run()
    #     em.evaluate()
    # index_of_best_means = k_means_cosine.evaluate()
    # printer.ClassificationPrinter(k_means_cosine.meanss[index_of_best_means], gc.dictionary).output()

    # for num_means in range(28, 37, 4):
    #     k_means_cosine = kmeans_managers.KMeansClustererManager(tfifd_docs=tfidf_docs,
    #                                                             svd_dimensions=80,
    #                                                             distance_func=nltk.cluster.util.cosine_distance,
    #                                                             num_means=num_means)
    #     k_means_cosine.run()
    #     index_of_best_means = k_means_cosine.evaluate()
    # printer.ClassificationPrinter(k_means_cosine.meanss[index_of_best_means], gc.dictionary).output()

    # for num_means in range(28, 37, 4):
    #     clusterer = kmeans_managers.BisectingKMeansClusteringManager(tfifd_docs=tfidf_docs,
    #                                                                  svd_dimensions=80,
    #                                                                  num_means=num_means,
    #                                                                  distance_func=nltk.cluster.util.cosine_distance)
    #     clusterer.run()
    #     index_of_best_means = clusterer.evaluate()
    # printer.ClassificationPrinter(clusterer.meanss[index_of_best_means], gc.dictionary).output()

    clusterer = gaac_managers.GAACClustererManager(tfifd_docs=tfidf_docs,
                                                   svd_dimensions=80,
                                                   linkage_method="weighted",
                                                   distance_threshold=0.55,
                                                   dict=gc.dictionary)
    clusterer.run()
    index_of_best_means = clusterer.evaluate()
    # printer.ClassificationPrinter(clusterer.meanss[index_of_best_means], gc.dictionary).output()

    # k_means_euklidian = KMeansClusterer(tfifd_docs=tfidf_docs, distance_func=nltk.cluster.util.euclidean_distance)
    # k_means_euklidian.run()
    # index_of_best_means = k_means_euklidian.evaluate()
    # printer.ClassificationPrinter(k_means_euklidian.meanss[index_of_best_means], gc.dictionary).output()
    # k_means_pearson = KMeansClusterer(tfifd_docs=tfidf_docs, distance_func=util.pearson_correlation)
    # k_means_pearson.run()
    # index_of_best_means = k_means_pearson.evaluate()
    # printer.ClassificationPrinter(k_means_pearson.meanss[index_of_best_means], gc.dictionary).output()