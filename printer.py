import gensim
import numpy


class ClassificationPrinter(object):
    def __init__(self, means, dictionary):
        self.means = means
        self.dictionary = dictionary

    def output(self):
        for mean in self.means:
            print "Pairwise similarity " + str(numpy.linalg.norm(mean, ord=2) ** 2)
            mean_axes = gensim.matutils.full2sparse(mean, 0.018)
            mean_axes = sorted(mean_axes, key=lambda axe: axe[1])
            for token_id, tfidf_rate in mean_axes:
                print self.dictionary[token_id], " ", tfidf_rate
            print "-------------------"