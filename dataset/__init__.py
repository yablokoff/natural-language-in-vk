# -*- coding: utf-8 -*-

from HTMLParser import HTMLParser
import codecs
import re
from gensim import corpora, models
import nltk


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ' '.join(self.fed)


class DatasetCreator(object):
    """
    Group clustering.
    """
    def __init__(self, source_file="../sources/short_groups.txt", below_rank=3, above_rank=0.25):
        self.group_names = []
        self.source_file = source_file
        self.below_rank = below_rank
        self.above_rank = above_rank
        try:
            self.dictionary = corpora.Dictionary.load("../sources/groupsDict.saved")
        except IOError:
            self.dictionary = self.create_dictionary()
        try:
            self.corpus = corpora.MmCorpus("../sources/groupsCorpus.mm")
        except IOError:
            self.corpus = self.create_corpus(self.dictionary)
        self.model = self.create_model(self.corpus)

    def _create_group_names(self):
        stop_words_list = []
        with open("../sources/stop_words_ru.txt", "r") as stop_words:
            for stop_word in stop_words:
                stop_words_list.append(stop_word.decode("utf-8").strip())
        with codecs.open(self.source_file, "r", "utf-8") as groups:
            i = 0
            nonwords_pattern = re.compile(ur'[^A-Za-zА-Яа-я\s\.-]+', re.UNICODE)
            urls_pattern = re.compile(ur'(((file|gopher|news|nntp|telnet|http|ftp|https|ftps|sftp)://)|(www\.))'
                                      ur'+(([a-zA-Z0-9\._-]+\.[a-zA-Z]{2,6})|([0-9]{1,3}\.[0-9]{1,3}\.'
                                      ur'[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9\&amp;%_\./-~-]*)?')
            stemmer = nltk.stem.SnowballStemmer("russian")
            while True:
                i += 1
                group_name = groups.readline()
                if not group_name:
                    break
                stripper = MLStripper()
                stripper.feed(group_name)
                group_name = stripper.get_data()
                group_name = urls_pattern.sub(" ", group_name)
                group_name_wo_trash = nonwords_pattern.sub(" ", group_name)
                group_name = re.sub(" +", " ", group_name_wo_trash).strip()
                group_name_parsed = []
                for sent in nltk.sent_tokenize(group_name):
                    for word in nltk.word_tokenize(sent):
                        if not word.lower() in stop_words_list:
                            group_name_parsed.append(stemmer.stem(word.lower()))
                self.group_names.append(group_name_parsed)
                if i > 971:
                    break
        return self.group_names

    def create_dictionary(self):
        self._create_group_names()
        dictionary = corpora.Dictionary(self.group_names)
        dictionary.filter_extremes(no_below=self.below_rank, no_above=self.above_rank)
        dictionary.save("../sources/groupsDict.saved")
        return dictionary

    def create_corpus(self, dictionary):
        if not self.group_names:
            self._create_group_names()
        corpus = [dictionary.doc2bow(group_name) for group_name in self.group_names]
        corpora.MmCorpus.serialize("../sources/groupsCorpus.mm", corpus)
        return corpus

    def create_model(self, corpus):
        tfidf = models.TfidfModel(corpus)
        return tfidf