from nltk.cluster.kmeans import KMeansClusterer
import numpy


class KMeansBisectingClusterer(KMeansClusterer):
    """
    Класс решения задачи кластеризации методом k-средних.
    Расширение стандартного метода k-средних в библиотеке nltk.
    """

    def __init__(self, num_means, distance, bisecting_repeats=5,
                 repeats=1, conv_test=1e-6, initial_means=None,
                 normalise=False, svd_dimensions=None,
                 rng=None, avoid_empty_clusters=False):
        """
        @param num_means: количество кластеров
        @param distance: функция расстояния
        @param bisecting_repeats: количество пробных рассечений кластера
        @param repeats: общее количество попыток кластеризации
        """
        self._bisecting_repeats = bisecting_repeats
        super(KMeansModBisectingClusterer, self).__init__(
            num_means, distance, repeats, conv_test,
            initial_means, normalise, svd_dimensions,
            rng, avoid_empty_clusters
        )

    def _calculate_SSE(self, clusterer, vectors):
        """
        Функция расчета средней квадратичной ошибки для кластерного решения.
        """
        squared_error = 0
        for vector in vectors:
            closest_mean = clusterer.means()[clusterer.classify_vectorspace(vector)]
            dist = self._distance(closest_mean, vector) ** 2
            squared_error += dist
        return squared_error

    def cluster_vectorspace(self, vectors, trace=False):
        """
        Функция кластеризации. На выходе — массив медиан кластеров в self._means.
        
        @param vectors: входящий массив документов.
        """
        self._means = [numpy.array([])]
        while len(self._means) < self._num_means:
            # рассекаем первый кластер в списке уже построенных
            # @bisecting_cluster_mean — медиана рассекаемого кластера
            bisecting_cluster_mean = self._means[0]
            if not bisecting_cluster_mean.size:
                # в первой итерации кластером будут все документы
                cluster_vectors = vectors
            else:
                # соберем в @cluster_vectors все документы рассекаемого кластера
                cluster_vectors = []
                for vector in vectors:
                    index = self.classify_vectorspace(vector)
                    if not index:
                        cluster_vectors.append(vector)
            min_SSE = 10 ** 9
            min_SSE_means = None
            for i in range(self._bisecting_repeats):
                # заданное количество раз рассечем кластер с помощью стандартного
                # алгоритма кластеризации k-средних
                bisecting_clusterer = KMeansClusterer(num_means=2, distance=self._distance)
                bisecting_clusterer.cluster_vectorspace(cluster_vectors)
                # рассчитаем суммарную квадратичную ошибку
                SSE = self._calculate_SSE(bisecting_clusterer, cluster_vectors)
                if SSE < min_SSE:
                    # сохраним медианы двух получившихся кластеров, если ошибка минимальна
                    min_SSE = SSE
                    min_SSE_means = bisecting_clusterer.means()
            self._means = self._means[1:]
            for mean in min_SSE_means:
                self._means.append(mean)

