__author__ = 'yablokoff'

import numpy
import pickle


class ModClustererMixin(object):

    def cluster(self, vectors, assign_clusters=False, trace=False):
        assert len(vectors) > 0

        # normalise the vectors
        if self._should_normalise:
            vectors = map(self._normalise, vectors)

        # use SVD to reduce the dimensionality
        if self._svd_dimensions and self._svd_dimensions < len(vectors[0]):
            try:
                svd_vectors_file = open("../sources/SVDVectors.pk", "rb")
                SVD_matrices = pickle.load(svd_vectors_file)
                S = SVD_matrices["S"]
                T = SVD_matrices["T"]
                Dt = SVD_matrices["Dt"]
            except IOError:
                [u, d, vt] = numpy.linalg.svd(numpy.transpose(numpy.array(vectors)))
                S = d[:self._svd_dimensions] * \
                    numpy.identity(self._svd_dimensions, numpy.float64)
                T = u[:,:self._svd_dimensions]
                Dt = vt[:self._svd_dimensions,:]
                svd_vectors_file = open("../sources/SVDVectors.pk", "wb")
                pickle.dump({"S": S, "T": T, "Dt": Dt}, svd_vectors_file)
            vectors = numpy.transpose(numpy.dot(S, Dt))
            self._Tt = numpy.transpose(T)

        # call abstract method to cluster the vectors
        self.cluster_vectorspace(vectors, trace)

        # assign the vectors to clusters
        if assign_clusters:
            # print self._Tt, vectors
            return [self.classify(vector) for vector in vectors]
